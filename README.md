# AAS Generator

Shell script automating the compilation of AAS files required for bot navigation in BSP maps in ioQ3 based games.

## How Do I Get The AAS?

### Usage:

	./aasgen.sh [-bspc switches] [pk3s/dirs] [to:path]

### -bspc switches
The switches used by BSPC to generate the AAS
([Full list available here](http://linradiant.intron-trans.hu/mbspc.html))
>Example: -forcesidesvisible -optimize

### pk3s/dirs
List the pk3s files/directories you want to analyze. BSPC will process any .bsp found within the /maps/ directory of the pk3s found.
Subdirectories are not parsed.
>Example: aa.pk3 dir1 dir2/bb.pk3

### to:_path_
specify the name of the pk3 you want to store the AAS in. Default is Bot_Support (no need to specify the extension).
>Example: to:_AAS\_Mapname_

## Additional info

* If no pk3s/dir is specified, the script will analyze all the pk3s present in the directory containing the script (subdirectories are not parsed).

* An additional log file will be generated for each pk3 processed.
