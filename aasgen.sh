#!/bin/bash

# Mr.Yeah!'s attempt at an automated bot support script
#
#	usage: aasgen.sh [-mbspc args] [path/to/map.pk3] [to:{dest}]
#
#			- will process the pk3s listed (filename or/and directories, no subs) and generate
#			an .aas for every bsp without an already attached .aas found inside the maps/ directory according to the bspc switches specified.
#			The resulting aas files will be stored in a .pk3 named {dest}.pk3 (default: Bot_Support)
#			An additional log file will be generated for each pk3 processed.
#
#			default (called without args): processes all the pk3s within the directory containing the script
#			(does not check for subdirectories)
#
#	requires:
#
#				-mbspc


AASGEN_BSPC_EXEC=./mbspc.x86_64
[[ ! -e $AASGEN_BSPC_EXEC ]] && echo "[Error] BSPC executable not found" && exit 1
[[ ! -x $AASGEN_BSPC_EXEC ]] && echo "[Error] Could not execute BSPC" && exit 2

mkdir ./maps/
shopt -s extglob
AASGEN_PK3PATH="!(zUrT*).pk3"
AASGEN_ARGS="-optimize -forcesidesvisible"
AASGEN_DEST="Bot_Support"

bspc()
{
	for map in $AASGEN_PK3PATH
	do
		AASGEN_EXISTING=$(unzip -l $map | grep "maps/.*\.aas")
		if [[ ! -z $AASGEN_EXISTING ]]; then
			$AASGEN_BSPC_EXEC  -bsp2aas "$map"/maps/*.bsp $AASGEN_ARGS -output ./maps/
			mv ./bspc.log ./"$map".log
		fi
	done
}


if [[ "$#" -eq 0 ]]; then
		bspc
	elif [[ "$#" -eq 1 && "$1" =~ ^to:[^.]*$ ]]; then
		AASGEN_DEST=`echo "$1" | sed -e 's/to://g'`
		bspc
	else
		while [[ "$1" =~ ^-[^.]*$ ]] 
			do
				AASGEN_ARGS="$AASGEN_ARGS $1"
			shift
			done
		while [[ ! "$1" = "" ]]
			do
				if [[ "$1" =~ ^.*.pk3$ ]]; then
					AASGEN_PK3PATH="$1"
					bspc
				elif [[ -d "$1" ]]; then
					AASGEN_PK3PATH="$1/*.pk3"
					bspc
				elif [[ "$1" =~ ^to:[^.]*$ ]]; then
					AASGEN_DEST=`echo "$1" | sed -e 's/to://g'`
				fi
				shift
			done
		if [[ "$AASGEN_PK3PATH" = "*.pk3" ]]; then
			bspc
		fi
fi

zip -9 -r -q -m "$AASGEN_DEST".pk3 ./maps
